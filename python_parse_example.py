"""
This file shows incorrect syntax highlighting with some previous versions of the Gitlab parser.
I wrote this as a minimal test case for a bug report.
"""


r'\/'
print(1234)  # this is valid python
'print(1234)'  # this is a string not python
